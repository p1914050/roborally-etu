#include<iostream>

#include "board.hpp"
#include "climits"
#include "graphe.hpp"


Graphe::Graphe(Board board){
    for(std::pair<Location, Board::TileType> tile : board.tiles) {
        for(int i=0; i<4; i++){
            Robot robot{tile.first, (Robot::Status) i};
            Noeud n = Noeud(tile.first, (int) tile.second);
            n.trouver_arete(board, (Robot::Status) i);
            list_noeuds.push_back(n);
            noeuds[robot] = &list_noeuds.back();
        }
    }
}

unordered_map<Robot, pair<Robot,int>, RobotHash> Graphe::find_path(Location start, Location end, Robot::Status status){
    Robot robot_to_move{start, status};
    Robot robot_end{end, status};
    unordered_map<Robot, int, RobotHash> distances;
    distances[robot_to_move] = 0;
    unordered_map<Robot, pair<Robot,int>, RobotHash> previous_case;
    if(noeuds.find(robot_to_move) != noeuds.end() && noeuds.find(robot_end) != noeuds.end() && !(start == end) && status <= Robot::Status::DEAD){
        unordered_map<Robot, Noeud*, RobotHash> seen;
        while (seen.size() < list_noeuds.size() && !(robot_to_move.location == end) && noeuds.find(robot_to_move) != noeuds.end()){
            for(int i=0; i<7; i++){
                Robot next = noeuds.at(robot_to_move)->aretes[i];
                int poids_path = noeuds.at(robot_to_move)->poids_arete[i];
                if(next.status <= Robot::Status::DEAD && noeuds.find(next) != noeuds.end()){
                    if(distances.find(next) != distances.end() ){
                        if(distances[robot_to_move] + poids_path < distances[next]){
                            distances[next] = distances[robot_to_move] + poids_path;
                            previous_case[next] = {robot_to_move, i};                  
                        }
                    }
                    else{
                        distances[next] = poids_path + distances[robot_to_move];
                        previous_case[next] = {robot_to_move, i}; 
                    }
                }
            }
            Robot shortest_robot;
            int shortest_path = INT_MAX;
            for(std::pair<Robot, int> distance : distances) {
                if (seen.find(distance.first) == seen.end() && distance.first.status <= Robot::Status::DEAD){
                    if(distance.second < shortest_path || (distance.first.location == end && distance.second == shortest_path)){
                        shortest_path = distance.second;
                        shortest_robot = distance.first;
                    }
                }
            }
            seen[robot_to_move] = noeuds.at(robot_to_move);
            robot_to_move = shortest_robot;
        }
    }
    else{
        cout<<"La case de départ ou d'arrivée n'est pas dans le graphe ou le robot n'est pas en état de se déplacer"<<endl;
    }
    return previous_case;
}

void Graphe::display_path(Location start, Location end, Robot::Status status, unordered_map<Robot, pair<Robot,int>, RobotHash> path){
    Robot robot_start{start, status};
    Robot robot_to_move = {Location(-1,-1), status};
    for (int i = 0; i<4; i++){
        if (path.find(Robot{end, (Robot::Status) i}) != path.end()){
            robot_to_move = Robot{end, (Robot::Status) i};
        }
    }
    if(robot_to_move.location == Location(-1,-1)){
        cout<<"Aucun chemin possible entre ces deux cases"<<endl;
    }
    else{
        while (!(robot_to_move.location == robot_start.location) || robot_start.status != robot_to_move.status){
            cout<<"("<<robot_to_move.location.line<<","<<robot_to_move.location.column<<","<<(int)robot_to_move.status<<") from"<<endl;
            cout<<"("<<path.at(robot_to_move).first.location.line<<","<<path.at(robot_to_move).first.location.column<<","<<(int)path.at(robot_to_move).first.status<<") : "<<path.at(robot_to_move).second<<endl;
            robot_to_move = path.at(robot_to_move).first;
        }
    }
}
