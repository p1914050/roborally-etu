#ifndef ROBORALLY_GRAPHE_HPP_
#define ROBORALLY_GRAPHE_HPP_

#include <unordered_map>
#include <vector>

#include "board.hpp"
#include "noeud.hpp"


using namespace std ;
using namespace RR ;

struct Graphe {
    unordered_map<Robot, Noeud*, RobotHash> noeuds;
    vector<Noeud> list_noeuds;

    Graphe(Board board);
    unordered_map<Robot, pair<Robot,int>, RobotHash> find_path(Location start, Location end, Robot::Status status);
    void display_path(Location start, Location end, Robot::Status status, unordered_map<Robot, pair<Robot,int>, RobotHash> path);
} ;

#endif