#include "climits"

#include "board.hpp"
#include "noeud.hpp"

using namespace RR;

Noeud::Noeud(){
    type_case = 51;
    coordinates = Location(-1,-1);
}

Noeud::Noeud(RR::Location location, int tile){
    type_case = tile;
    coordinates = location;
}

void Noeud::trouver_arete(Board board, Robot::Status status){
    for (int j=0; j<7; j++){
        Robot robot_to_move{coordinates, status};
        board.play(robot_to_move, (Robot::Move) j);
        if(robot_to_move.status >= Robot::Status::DEAD){
            poids_arete[j]=INT_MAX;
            robot_to_move.location = coordinates;
            aretes[j] = robot_to_move;
        }
        else{
            switch(j){
                case 0 :                        //Avancer de 1 case
                    //poids_arete[j]=1;
                    //aretes[j] = robot_to_move;
                case 1 :                        //Avancer de 2 case
                    //poids_arete[j]=1;
                    //aretes[j] = robot_to_move;
                case 2 :                        //Avancer de 3 case
                    //poids_arete[j]=1;
                    //aretes[j] = robot_to_move;
                case 3 :                        //Reculer de 1 case
                    //poids_arete[j]=1;
                    //aretes[j] = robot_to_move;
                case 4 :                        //Tourner à gauche
                    //poids_arete[j]=1;
                    //aretes[j] = robot_to_move;
                case 5 :                        //Tourner à droite
                    //poids_arete[j]=1;                         //Pour personnaliser les poids, retirez les commmentaires
                    //aretes[j] = robot_to_move;                //et changez la valeur de poids_arete
                case 6 :                        //Demi tour
                    poids_arete[j]=1;
                    aretes[j] = robot_to_move;              // Poids de 1 par défaut pour toutes les actions
            }  
        }
    }    
}
