#ifndef ROBORALLY_NOEUD_HPP_
#define ROBORALLY_NOEUD_HPP_

#include "board.hpp"

using namespace std;
using namespace RR;

struct Noeud {
    int type_case;
    Location coordinates;
    Robot aretes[7];
    int poids_arete[7];

    Noeud();
    Noeud(Location location, int tile);

    void trouver_arete(Board board, Robot::Status status);
} ;

#endif