#include "board.hpp"
#include "graphe.hpp"


#include <iostream>
#include <assert.h> 

int main(int argc, char *argv[]) {
  RR::Board b("board.txt") ;
  b.save("/tmp/cpy.txt") ;
  Graphe g(b);
  RR::Location l_deb;
  RR::Location l_fin;
  int status;

  if(argc==1){
    l_deb.line = 0; 
    l_deb.column = 1;
    status = 1;  
    l_fin.line =1;
    l_fin.column = 1;
  }
  else if(argc == 6){
    l_deb.line = stoi(argv[1]); 
    l_deb.column = stoi(argv[2]);
    status = stoi(argv[3]);  
    l_fin.line = stoi(argv[4]);
    l_fin.column = stoi(argv[5]);
  }
  else{
    cout<<"Nombre d'arguments non valide"<<endl;
    return 0;
  }


  unordered_map<Robot, pair<Robot,int>, RobotHash> path = g.find_path(l_deb,l_fin, (RR::Robot::Status) status);
  g.display_path(l_deb, l_fin, (RR::Robot::Status) status, path);

  return 0 ;
}
