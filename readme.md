# Projet Roborally

Ce projet vise à créer un joueur automatique pour le jeu [Roborally](https://fr.wikipedia.org/wiki/RoboRally). 
Le sujet est disponible en pdf [sur la page du cours](https://liris.cnrs.fr/vincent.nivoliers/lifap6/Supports/Projet/roborally.pdf).

# How to use
Pour exécuter le code, compilez puis lancez l'exécutable test_board en précisant les positions de départ, les positions d'arrivée et le statut du robot au départ.

Par exemple : 
    ```
        ./test_board 0 1 3 2 4
    ```

Le robot à la case (0,1) avec le statut 3 (Sud) vers la case (2,4).
Par défaut, on veut le plus court chemin entre la case (0,1) avec le statut 1(Nord) et la case (1,1).

Notre algorithme fonctionne sous un algorithme de Dijkstra, vous pouvez paramétrer les poids de chaque type de déplacement dans le fichier noeud.cpp dans la fonction trouver_arete. Par défaut, toutes les arêtes ont un poids de 1;

Pour comprendre notre affichage voici un exemple avec le chemin le plus court du robot en (0,1) qui pointe vers l'Ouest( = 2) vers la case (2,4): 
```
    (2,4,0) from
    (4,2,0) : 1
    (4,2,0) from
    (0,1,3) : 2
    (0,1,3) from
    (0,1,2) : 4
```

Il faut regarder de bas en haut, deux par deux , la dernière ligne étant la case de départ. Prenons les lignes suivantes :
```
    (0,1,3) from
    (0,1,2) : 4 
``` 

Ces deux lignes peuvent être traduites de cette manière : 

    >>>
    Le robot en (0,1) avec le statut (2) effectue le déplacement 4 (Tourner à gauche) pour finir sur la case (0,1) avec le statut (3)
    >>>

